# KNmisc 0.1.0

-   Initial submission.

# knutils 0.1.0

-   5.07.2023: consistent license (GLP\>=3) and rename to knutils. The license and name change should make this more aligned with David Miller's dlmutils package [(URL)](https://gitlab.bioss.ac.uk/dmiller/dlmutils){.uri}
