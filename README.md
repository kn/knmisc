
# knutils

<!-- badges: start -->
<!-- badges: end -->

The goal of knutils is to provide various convenience functions for data exploration and analysis.

## TODO
* use different residual types depending on model input, align choices with defaults of the R packages used to fit the models


## Installation

You can install the development version of knutils like so:

``` r
# FILL THIS IN! HOW CAN PEOPLE INSTALL YOUR DEV PACKAGE?
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(knutils)
## basic example code
```

